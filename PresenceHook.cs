﻿using System;
using DiscordRpc;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.DiscordRichPresence {
    public class PresenceHook : IModHookV2 {
        private DateTime _start = DateTime.MinValue;
        private RichPresence _client;

        public void Dispose() {
            _client?.Shutdown();
            _client?.Dispose();
            _client = null;
        }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }
        public void GameContextInitializeAfter() { }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            if (!universe.Server) {
                if (_client == null) {
                    _client = new RichPresence();
                    _start = DateTime.Now;

                    _client.Initialize("460171330810281994");
                }

                var builder = new RichPresenceBuilder();

                var day = universe.DayNightCycle().Day + 1;

                var year = 1;

                while (day > 64) {
                    year++;
                    day = day - 64;
                }

                while (day > 16) {
                    day = day - 16;
                }

                builder.WithTimeInfo(_start, null);
                builder.WithState("Day: " + day + " Season: " +
                                  ClientContext.LanguageDatabase
                                      .GetTranslationString(SeasonHelper
                                          .FromInt(universe.DayNightCycle()
                                              .GetSeason()).GetCode()) + " Year: " + year, "Farming on " + universe.ServerName());

                _client.Update(builder);
            }
        }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() {
        }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }

        public void ClientContextDeinitialize() {
        }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() {
            _client?.Shutdown();
            _client?.Dispose();
            _client = null;
        }
    }
}
